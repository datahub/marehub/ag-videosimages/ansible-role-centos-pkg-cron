# centos pkg cron role
Ansible role to deploy and run
[centos-package-cron](https://github.com/wied03/centos-package-cron).

Written for Centos 7 only.

It does not (yet) set up a cron job. Its current intention is to gather the state on the ansible controller.

## variables

See `defaults/main.yml` and `vars/main.yml`.


## usage

```
- name: show usage of centos pkg cron role
  hosts: testgroup

  roles:
    - role: centos_pkg_cron
      become: yes
```

## License

MIT

## Author Information

This role was created in 2022 by [Dirk Sarpe][author] aka [dirks at GitHub][github], working for [GEOMAR Helmholtz Centre for Ocean Research Kiel][geomar].

[author]: https://www.geomar.de/dsarpe
[geomar]: https://www.geomar.de/
[github]: https://github.com/dirks

